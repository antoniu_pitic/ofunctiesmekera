#include "functiileMele.h"

int f(int x, int y)
{
	if (x > 0 && y < 0) {
		return x + y;
	}
	
	if (x > 0 && y >= 0) {
		return minim(x, y);
	}

	if (x <= 0) {
		return maxim(x, y+x-3);
	}
}

int maxim(int a, int b)
{
	if (a > b) {
		return a;
	}
	else {
		return b;
	}
}

int minim(int a, int b)
{
	return a + b - maxim(a, b);
}
